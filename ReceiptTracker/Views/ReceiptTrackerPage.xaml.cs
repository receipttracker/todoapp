﻿using System.Diagnostics;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReceiptTracker.Views
{
    public partial class ReceiptTrackerPage : ContentPage
    {
        public ReceiptTrackerPage()
        {
            Debug.WriteLine($"****{this.GetType().Name}; ctor");
            InitializeComponent();
        }
    }
}
