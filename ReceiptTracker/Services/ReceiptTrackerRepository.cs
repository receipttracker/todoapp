﻿using System;
using ReceiptTracker.DependencyServices;
using System.Diagnostics;
using SQLite;
using ReceiptTracker.Models;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Generic;


namespace ReceiptTracker.Services
{
    public class ReceiptTrackerRepository : IReceiptTrackerRepository 
    {
        private IFileAccessService _fileAccessService;
        private SQLiteAsyncConnection _sqliteConnection;

        public ReceiptTrackerRepository(IFileAccessService fileAccessService)
        {
            _fileAccessService = fileAccessService;

            var databaseFilePath = _fileAccessService.GetSqLiteDatabasePath("ReceiptTracker.db3");
            _sqliteConnection = new SQLiteAsyncConnection(databaseFilePath);
            CreateTablesSynchronously();


            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ReceiptTrackerRepository)}: ctor. databaseFilePath={databaseFilePath}");
        }

        private void CreateTablesSynchronously()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(CreateTablesSynchronously)}");

            _sqliteConnection.CreateTableAsync<ToDoTopic>().Wait();

        
        }

        public async Task<int> DeleteToDoTopic(ToDoTopic ToDoTopicToDelete)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DeleteToDoTopic)}:  {ToDoTopicToDelete}");

            return ToDoTopicToDelete.Id = await _sqliteConnection.DeleteAsync(ToDoTopicToDelete);


        }

        public async Task<int> SaveToDoTopic(ToDoTopic ToDoTopicToSave)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(SaveToDoTopic)}:  {ToDoTopicToSave}");

            try
            {
                if (ToDoTopicToSave.Id == 0) // an Id of 0 indicates a new item, not yet saved.
                {
                    // This must be a NEW topic to insert
                    ToDoTopicToSave.Id = await _sqliteConnection.InsertAsync(ToDoTopicToSave);
                }
                else
                {
                    // This must be an EXISTING topic to update
                    await _sqliteConnection.UpdateAsync(ToDoTopicToSave);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ToDoTopicToSave)}:  EXCEPTION!! {ex}");
            }

            return ToDoTopicToSave.Id;
        }

        public async Task<ObservableCollection<ToDoTopic>> GetAllToDoTopics()
        {
            var allTopics = new ObservableCollection<ToDoTopic>();

            try
            {
                List<ToDoTopic> topicList = await _sqliteConnection.Table<ToDoTopic>().ToListAsync();
                allTopics = new ObservableCollection<ToDoTopic>(topicList);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetAllToDoTopics)}:  EXCEPTION!! {ex}");
            }

            return allTopics;
        }

    }
}
