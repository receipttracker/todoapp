﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ReceiptTracker.Models;
using System.Diagnostics;

namespace ReceiptTracker.Services
{
    public interface IReceiptTrackerRepository
    {
        Task<int> SaveToDoTopic(ToDoTopic ToDoTopicToSave);
        Task<int> DeleteToDoTopic(ToDoTopic ToDoTopicToDelete);
        Task<ObservableCollection<ToDoTopic>> GetAllToDoTopics();
    }
}