﻿using System;
using SQLite;
using System.Diagnostics;
namespace ReceiptTracker.Models                        
{
    [Table("ToDoTopic")]
    public class ToDoTopic
    {      
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }

            [MaxLength(250)]
            public string BriefDescription { get; set; }

            [MaxLength(1000)]
            public string Details { get; set; }

            public override string ToString()
            {
                return $"Id:{Id}, BriefDescription:{BriefDescription}, Details:{Details}";
            }

    }
}
