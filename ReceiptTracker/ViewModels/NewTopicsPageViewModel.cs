﻿using System;
using System.Diagnostics;
using Prism.Commands;
using Prism.Navigation;
using ReceiptTracker.Services;
using ReceiptTracker.Models;
using Prism.Services;
using Plugin.TextToSpeech.Abstractions;
using Plugin.TextToSpeech;

namespace ReceiptTracker.ViewModels
{
    public class NewTopicsPageViewModel : ViewModelBase
    {
        private IReceiptTrackerRepository _receiptTrackerRepository;


        public DelegateCommand SaveTopicCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }
        public DelegateCommand DeleteCommand { get; set; }
        public DelegateCommand SpeakCommand { get; set; }

        private ToDoTopic _thisToDoTopic;
        public ToDoTopic ThisToDoTopic
        {
            get { return _thisToDoTopic; }
            set { SetProperty(ref _thisToDoTopic, value); }
        }

        private string _toDoTopicIdText;
        public string ToDoTopicIdText
        {
            get { return _toDoTopicIdText; }
            set { SetProperty(ref _toDoTopicIdText, value); }
        }

        public NewTopicsPageViewModel(INavigationService navigationService,
                                      IReceiptTrackerRepository receiptTrackerRepository)
            : base(navigationService)
        {

            Title = "To Do Topic";
            _receiptTrackerRepository = receiptTrackerRepository;
            SaveTopicCommand = new DelegateCommand(OnSaveTopicTapped);
            CancelCommand = new DelegateCommand(OnCancelTapped);
            DeleteCommand = new DelegateCommand(OnDeleteTapped);
            SpeakCommand = new DelegateCommand(OnSpeakTapped);
        }


        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters.ContainsKey("ToDoTopicKey"))
            {
                Title = "Details";
                ThisToDoTopic = parameters["ToDoTopicKey"] as ToDoTopic;
            }
            else
            {
                Title = "New ToDo-Topic";
                ThisToDoTopic = new ToDoTopic();
            }

            ToDoTopicIdText = $"ToDo Topic Id: {ThisToDoTopic.Id}";

        }

        private async void OnDeleteTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDeleteTapped)}");
            ThisToDoTopic.Id = await _receiptTrackerRepository.DeleteToDoTopic(ThisToDoTopic);
            await NavigationService.GoBackAsync();
        }

        private void OnCancelTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCancelTapped)}");
            NavigationService.GoBackAsync();
        }

        private async void OnSaveTopicTapped()
        {
           
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSaveTopicTapped)}");
            ThisToDoTopic.Id = await _receiptTrackerRepository.SaveToDoTopic(ThisToDoTopic);
            await NavigationService.GoBackAsync();

        }

        private async void OnSpeakTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSpeakTapped)}");
            await CrossTextToSpeech.Current.Speak(ThisToDoTopic.BriefDescription + "   " + ThisToDoTopic.Details);
        }


    }
}
