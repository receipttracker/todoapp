﻿using System.Diagnostics;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prism.Navigation;
using Prism.Commands;
using System;
using Xamarin.Forms;
using ReceiptTracker.Views;
using Prism.Services;

namespace ReceiptTracker.ViewModels
{
    public class ReceiptTrackerPageViewModel : BindableBase, INavigationAware
    {

        INavigationService _navigationService;
        IPageDialogService _pageDialogService;

        public DelegateCommand NavToAddPhotoPageCommand { get; set; }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }  
        }


        public ReceiptTrackerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}:  ctor");
            Title = "Let's Do It";
            _navigationService = navigationService;
            _pageDialogService = pageDialogService;
            NavToAddPhotoPageCommand = new DelegateCommand(OnNavToAddPhotoPage);

        }

       

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");

        }

        private async void OnNavToAddPhotoPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavToAddPhotoPage)}");

            bool UsersResponse = await _pageDialogService.DisplayAlertAsync("Alert!",
                                                   "Are you sure you want to get doing?",
                                                   "HELL YEA",
                                                    "NOOOOO");
            if(UsersResponse == true)
            {
                await _navigationService.NavigateAsync(nameof(AddPhotoPage));

            }



        }
    }
}