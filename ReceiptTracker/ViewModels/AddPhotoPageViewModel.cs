﻿using System;
using Prism.Mvvm;
using Xamarin.Forms;
using System.Diagnostics;
using ReceiptTracker.Models;
using ReceiptTracker.Services;
using ReceiptTracker.Views;
using Prism.Commands;
using Prism.Navigation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ReceiptTracker.ViewModels
{
    public class AddPhotoPageViewModel : ViewModelBase
    {
        private IReceiptTrackerRepository _receiptTrackerRepository;

        public DelegateCommand AddTopicCommand { get; set; }
        public DelegateCommand<ToDoTopic> TopicTappedCommand { get; set; }

        private ObservableCollection<ToDoTopic> _topics;
        public ObservableCollection<ToDoTopic> Topics
        {
            get { return _topics; }
            set { SetProperty(ref _topics, value); }
        }

        public AddPhotoPageViewModel(INavigationService navigationService,
                                IReceiptTrackerRepository receiptTrackerRepository)
            : base(navigationService)
        {
            _receiptTrackerRepository = receiptTrackerRepository;
            Title = "To Do List!";
            AddTopicCommand = new DelegateCommand(OnAddTopicTapped);
            TopicTappedCommand = new DelegateCommand<ToDoTopic>(OnTopicTapped);

        }

        private void OnTopicTapped(ToDoTopic topicTapped)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnTopicTapped)}:  {topicTapped}");

            NavigationParameters navParams = new NavigationParameters();
            navParams.Add("ToDoTopicKey", topicTapped);
            NavigationService.NavigateAsync(nameof(NewTopicsPage), navParams, false, true);
        }

        private async void RefreshToDoTopics()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshToDoTopics)}");

            Topics = await _receiptTrackerRepository.GetAllToDoTopics();
        }

        private async void OnAddTopicTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAddTopicTapped)}");

            await NavigationService.NavigateAsync(nameof(NewTopicsPage), null, true, true);
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            RefreshToDoTopics();
        }
    }
}

