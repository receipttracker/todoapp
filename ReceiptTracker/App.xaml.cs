﻿using System.Diagnostics;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using ReceiptTracker.Views;
using ReceiptTracker.ViewModels;
using Xamarin.Forms.Xaml;
using ReceiptTracker.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ReceiptTracker
{
    public partial class App : PrismApplication
    {
       
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override async void OnInitialized()
		{
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

           // NavigationService.NavigateAsync(nameof(ReceiptTrackerPage));
            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(ReceiptTrackerPage)}");
		}

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<ReceiptTrackerPage, ReceiptTrackerPageViewModel>();
            containerRegistry.RegisterForNavigation<AddPhotoPage, AddPhotoPageViewModel>();
            containerRegistry.RegisterForNavigation<NewTopicsPage, NewTopicsPageViewModel>();
            containerRegistry.RegisterSingleton<IReceiptTrackerRepository, ReceiptTrackerRepository>();


        }
		protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
            base.OnStart();
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            // Handle when your app sleeps
            base.OnSleep();
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            // Handle when your app resumes
            base.OnResume();
        }
    }
}
