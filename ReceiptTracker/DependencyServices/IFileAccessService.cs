﻿using System;
using System.Diagnostics;
namespace ReceiptTracker.DependencyServices
{
    public interface IFileAccessService
    {
        string GetSqLiteDatabasePath(string databaseName);

    }
}
