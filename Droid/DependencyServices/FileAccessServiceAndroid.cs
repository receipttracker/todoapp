﻿using System;
using System.IO;
using ReceiptTracker.DependencyServices;
using ReceiptTracker.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileAccessServiceAndroid))]
namespace ReceiptTracker.Droid.DependencyServices
{
    public class FileAccessServiceAndroid : IFileAccessService
    {


        public string GetSqLiteDatabasePath(string databaseName)
        {
            string personalFolderPath = Environment.GetFolderPath(
                Environment.SpecialFolder.Personal);
            var dbPath = Path.Combine(personalFolderPath, databaseName);
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(GetSqLiteDatabasePath)}:  returning:[{dbPath}]");
            return dbPath;
        }
    }
}
